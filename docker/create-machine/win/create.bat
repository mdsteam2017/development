@echo off
set /p name="Enter the Docker Machine name: "
docker-machine create --driver virtualbox --virtualbox-disk-size=40960 --virtualbox-cpu-count=1 --virtualbox-memory=6144 %name%