@echo off
set /p SWARM_NAME="Enter the swarm name (ex. my-little-hatchery): "

set SWARM_MANAGER=swarm-%SWARM_NAME%-manager
set SWARM_WORKER=swarm-%SWARM_NAME%-worker

set /p REMOVE_OLD="Enter Y in order to remove old virtul machines: "
IF /I "%REMOVE_OLD%"=="Y" (
    docker-machine rm -f %SWARM_MANAGER%
    docker-machine rm -f %SWARM_WORKER%
)

REM Create a manager and worker nodes
docker-machine create --engine-opt dns=8.8.8.8 --driver virtualbox --virtualbox-disk-size=40960 --virtualbox-cpu-count=1 --virtualbox-memory=2048 %SWARM_MANAGER%
docker-machine create --engine-opt dns=8.8.8.8 --driver virtualbox --virtualbox-disk-size=40960 --virtualbox-cpu-count=1 --virtualbox-memory=2048 %SWARM_WORKER%

REM Retrieve ip address of the manager node
set COMMAND='docker-machine ip %SWARM_MANAGER%'
FOR /F "tokens=*" %%g IN (%COMMAND%) DO (SET SWARM_MANAGER_IP=%%g)
echo IP address of manager is %SWARM_MANAGER_IP%

REM Retrieve ip address of the worker node
set COMMAND='docker-machine ip %SWARM_WORKER%'
FOR /F "tokens=*" %%g IN (%COMMAND%) DO (SET SWARM_WORKER_IP=%%g)
echo IP address of worker is %SWARM_WORKER_IP% 

REM Log into the first node and make it manager
docker-machine ssh %SWARM_MANAGER% "docker swarm init --advertise-addr %SWARM_MANAGER_IP%" > manager_create.log

REM Extrack token
FINDSTR /I /R ".*join.--token.*" manager_create.log > token_line.log
set /p TOKEN_STRING=<token_line.log

REM Add worker to the swarm
docker-machine ssh %SWARM_WORKER% "docker swarm join --token %TOKEN_STRING:~30,85% %SWARM_MANAGER_IP%"

REM Remove useless files
del /f token_line.log
del /f manager_create.log

REM Make manager's node default
set COMMAND='docker-machine env %SWARM_MANAGER%'
@FOR /f "tokens=*" %%i IN (%COMMAND%) DO %%i

REM Deploy the Portainer to the Swarm
docker stack deploy -c docker-compose.yml portainer

REM Debug information
echo ''
echo *******************************************************************
echo * DEPLOYMENT OF SWARM IS COMPLETED                                *
echo * IP OF MANAGER IS %SWARM_MANAGER_IP%                             *
echo * PORTAINER IS AVAILABLE AT http://%SWARM_MANAGER_IP%:9000        *
echo * DON'T FORGET TO FIX ENVIRONMENT VARIABLES                       *
echo *******************************************************************