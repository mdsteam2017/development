cls
docker volume create apt-proxy-data
docker container rm apt-proxy
docker image rm apt-proxy
docker build -t apt-proxy .
docker run --name apt-proxy -v apt-proxy-data:/var/cache/apt-cacher-ng -p 3142:3142 apt-proxy