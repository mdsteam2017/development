Development environment from scratch
=

This `docker-compose.yml` file contains the following services:

* Jenkins
* Archiva
* SonarQube
* Portainer
* Docker registry

Services could be accessed using the following endpoints:

* Jenkins - `http://host:9080/`, inside internal network available via `http://mds_jenkins:8080/`
* SonarQube - `http://host:9090/`, inside internal network available via `http://mds_sonar:9000/`
* Archiva - `http://host:9100`, inside internal network available via `http://mds_archiva:8080/`

SonarQube
=

Default login and password are `admin`.

Installation and update
=

In order to start the stack execute the following command: `docker-compose up -d`. 
If you want to update the existing containers just run `docker-compose down && docker-compose up -d`. 